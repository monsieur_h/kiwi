package com.monsieur_h.kiwi;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

public abstract class BackgroundElement extends Actor implements Comparable<BackgroundElement>{
	protected float depth;
	private Texture texture;
	private String textureName;
	protected boolean fading;
	
	public BackgroundElement(String textureFilename, float depth) {
		this.init(textureFilename, depth, true);
	}
	
	public BackgroundElement(String textureFilename, float depth, boolean fading){
		this.init(textureFilename, depth, fading);
	}
	
	protected void init(String textureFilename, float depth, boolean fading){
		this.depth = depth;
		this.fading = fading;
		this.textureName = textureFilename;
		this.setTexture(textureFilename);
		this.setWidth(this.texture.getWidth());
		this.setHeight(this.texture.getHeight());
		this.setScale(depth);
	}
	
	protected void setTexture(String texturePath){
		this.texture = new Texture(Gdx.files.internal("textures/"+texturePath));
	}
	
	public Texture getTexture(){
		return this.texture;
	}
	
	public void setDepth(float depth){
		this.depth = depth;
	}

	public float getDepth(){
		return this.depth;
	}
	
	public boolean getFading(){
		return this.fading;
	}
	
	public void setFading(boolean fading){
		this.fading = fading;
	}
	
	public void setFullHeight(boolean fullHeight){
		if(fullHeight){
			float scale = (Gdx.graphics.getHeight() - this.getY()) / this.texture.getHeight();
			this.setHeight(this.texture.getHeight() * scale);
			this.setWidth(this.texture.getWidth() * scale);
		}else{
			this.setHeight(this.texture.getHeight());
		}
	}
	
	@Override
	public void setScale(float scale){
		this.setHeight(this.texture.getHeight() * scale);
		this.setWidth(this.texture.getWidth() * scale);
	}
	
	public void setScale(float x, float y){
		this.setHeight(this.texture.getHeight() *x);
		this.setWidth(this.texture.getWidth() * y);
	}
	
	public abstract void draw(Batch batch, float parentAlpha);
	
	public abstract void moveBy(float x, float y);
	
	@Override
	public int compareTo(BackgroundElement o) {
		final int BEFORE = -1;
	    final int EQUAL = 0;
	    final int AFTER = 1;
	    
	    if(this.getDepth() > o.getDepth()){
	    	return AFTER;
	    }else if(this.getDepth() < o.getDepth()){
	    	return BEFORE;
	    }else{
	    	return EQUAL;
	    }
	}
	
	public String toString(){
		String str = "BackgroundElement with texture " + this.textureName + " pos ("+this.getX()+ ", "+this.getY()+")";
		return str;
	}
}
