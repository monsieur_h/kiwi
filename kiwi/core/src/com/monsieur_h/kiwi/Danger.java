package com.monsieur_h.kiwi;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;


public class Danger extends Seed{
	private final Color color = new Color(1, 0, 0, 1);
	private Texture texture;
	
	public Danger() {
		this.texture = new Texture(Gdx.files.internal("textures/rockGrass.png"));
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha){
//		super.draw(batch, parentAlpha);
		batch.draw(this.texture, this.getX(), this.getY());
		
	}
	
	public Color getDrawColor(){
		return this.color;
	}
}
