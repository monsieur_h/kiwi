package com.monsieur_h.kiwi;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;

public class DisposableBackgroundElement extends BackgroundElement{
	public DisposableBackgroundElement(String textureFilename, float depth){
		super(textureFilename, depth);
	}
	
	public DisposableBackgroundElement(String textureFilename, float depth,
			boolean fading) {
		super(textureFilename, depth, fading);
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		if(this.fading)
			batch.setColor(1 * depth, 1 * depth, 1 * depth, 1);
		
		batch.draw(this.getTexture(), this.getX(), this.getY(), this.getWidth(), this.getHeight());
		
		if(this.getRight() < Gdx.graphics.getWidth()){
			//TODO:dispose
		}
		batch.setColor(1, 1, 1, 1);
	}

	@Override
	public void moveBy(float x, float y) {
		this.setX(this.getX() + (x * this.depth));
		this.setY(this.getY() + (y * this.depth));
	}
}
