package com.monsieur_h.kiwi;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class GameScene extends Stage implements InputProcessor {

	private Hero myKiwi;
	private List<Seed> seeds;
	private List<Danger> dangers;
	private float speed = 500f;
	private ParallaxManager parallax;
	private int WIDTH;
	private int HEIGHT;
	private int MID_HEIGHT;
	private Vector2 IDEAL_SIZE;
	
	GameScene(){
		super();
		this.WIDTH = Gdx.graphics.getWidth();
		this.HEIGHT = Gdx.graphics.getHeight();
		this.MID_HEIGHT = HEIGHT / 3;
		this.IDEAL_SIZE = new Vector2(1920, 1080);
		
		Vector2 scale = new Vector2(WIDTH / IDEAL_SIZE.x, HEIGHT / IDEAL_SIZE.y);
		
		this.initObstacles(50);
		this.initBackground();
		
		myKiwi = new Hero(scale.y);
		myKiwi.setZIndex(1);
		myKiwi.setPosition(WIDTH / 2, HEIGHT / 3);
		myKiwi.setScale(scale.x, scale.y);
		
		this.addActor(myKiwi);
		
		
		
		
		
		
		
		
		Gdx.input.setInputProcessor(this);
	}
	
	private void initObstacles(int number){
		//TODO: remove this and use an obstacle generator object
		Random r  = new Random();
		
		
		//Instantiating seeds
		seeds = new ArrayList<Seed>();
		for(int i = 0 ; i < number ; i++){
			Seed s = new Seed();
			s.setX((float) (r.nextInt(WIDTH * 100 + 1)));
			s.setY((float) MID_HEIGHT);
			this.addActor(s);
			seeds.add(s);
		}
		
		//Instantiating dangers
		dangers = new ArrayList<Danger>();
		for (int i = 0; i < (number / 3) ; i++) {
			Danger d = new Danger();
			d.setX((float) (r.nextInt(WIDTH * 100 + 1)));
			d.setY((float) MID_HEIGHT);
			d.setZIndex(0);
			this.addActor(d);
			dangers.add(d);
		}
	}
	
	private void checkCollisions(){
		//Checking collisions
		
		//Seeds
		for(int i=0 ; i < this.seeds.size() ; i++){
			Seed s = seeds.get(i);
			if(myKiwi.collides(s.getX(), s.getY()) && !myKiwi.isFruit()){
				myKiwi.pickItem();
				s.eaten();
			}
		}
		
		//Dangers
		for (int i = 0; i < this.dangers.size() ; i++) {
			Danger d = dangers.get(i);
			if(myKiwi.collides(d.getX(), d.getY()) && !myKiwi.isFruit()){
				myKiwi.die();
				d.isEaten();
			}
		}
		
		//Deleting eaten seeds
		for(int i = this.seeds.size() - 1 ; i >= 0 ; i--){
			if(seeds.get(i).isEaten()){
				this.seeds.get(i).remove();
				this.seeds.remove(i);
			}
		}
	}
	
	@Override
	public void act(float delta){
		super.act(delta);
		this.checkCollisions();
		this.parallax.moveBy(delta * -this.speed, 0);
		if(myKiwi.isDead()){
			this.speed = 0;
		}
	}
	
	@Override
	public void draw() {
		parallax.drawBackground(super.getBatch());
		super.draw();
		parallax.drawForeground(super.getBatch());
	};
	
	
	public void initBackground(){
		parallax = new ParallaxManager();
		Random rand = new Random();
		BackgroundElement bg = new ReapeatedBackgroundElement("background.png", 0.05f, false);
		BackgroundElement ground = new ReapeatedBackgroundElement("groundDirt.png", 0.55f);
		BackgroundElement ground2 = new ReapeatedBackgroundElement("groundDirt.png", 0.99f);
		
		for (int i = 0; i < 10; i++) {
			BackgroundElement mountain1 = new DisposableBackgroundElement("rock.png", rand.nextFloat());
			mountain1.setY(MID_HEIGHT);
			mountain1.setX(rand.nextFloat() * WIDTH * 3);
			parallax.add(mountain1);
		}
		
		bg.setY(MID_HEIGHT);
		bg.setFullHeight(true);
		ground.setY(MID_HEIGHT);
		ground2.setY(MID_HEIGHT);
		ground2.setScale(0.2f);
		parallax.add(bg);
		parallax.add(ground);
		parallax.add(ground2);
	}
	
	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		if(button == Buttons.LEFT){
        	this.myKiwi.toggle_state();
        }
        return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		if(button == Buttons.LEFT){
        	this.myKiwi.toggle_state();
        }
        return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public float getSpeed(){
		return this.speed;
	}

}
