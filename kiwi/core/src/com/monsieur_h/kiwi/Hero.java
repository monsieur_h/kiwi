package com.monsieur_h.kiwi;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.AnimationStateData;
import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.SkeletonData;
import com.esotericsoftware.spine.SkeletonJson;
import com.esotericsoftware.spine.SkeletonRenderer;
import com.esotericsoftware.spine.SkeletonRendererDebug;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Hero extends Actor{
	SkeletonRenderer renderer;
	SkeletonRendererDebug debugRenderer;

	TextureAtlas atlas;
	Skeleton skeleton;
	AnimationState state;
	
	private boolean fruit = false;
	private boolean alive = true;
	private final float collisionRadius = 50;
	
	Hero(float scale){
		renderer = new SkeletonRenderer();
		atlas = new TextureAtlas(Gdx.files.internal("skeleton.atlas"));
		SkeletonJson json = new SkeletonJson(atlas);
		json.setScale(scale);
		SkeletonData skelData = json.readSkeletonData(Gdx.files.internal("skeleton.json"));
		
		skeleton = new Skeleton(skelData);
		skeleton.setFlipX(true);
		
		
		AnimationStateData stateData = new AnimationStateData(skelData);
		
		state = new AnimationState(stateData);
		
		state.setTimeScale(1.5f);
		
		stateData.setMix("to_bird", "run_bird", 0.25f);
		stateData.setMix("to_fruit", "run_fruit", 0.25f);
		
		stateData.setMix("run_bird", "pick", 0.15f);
		stateData.setMix("pick", "run_bird", 0.15f);
		
		stateData.setMix("run_bird", "die", 0.25f);
		
		this.setWidth(skelData.getWidth() * scale / 16);//16 : because !
		this.setHeight(skelData.getHeight() * scale / 16);
		
		state.setAnimation(0, "run_bird", true);
	}
	
	@Override
	public void draw (Batch batch, float parentAlpha) {
		renderer.draw(batch, skeleton);
	}
	
	@Override
	public void act(float delta) {
		state.update(delta);
		state.apply(skeleton);
		skeleton.updateWorldTransform();
	};
	
	@Override
	public void setPosition (float x, float y) {
		super.setPosition(x, y);
		this.skeleton.setPosition(x, y);
	}
	
	@Override
	public void setX(float x){
		super.setX(x);
		this.skeleton.setX(x);
	}
	
	@Override
	public void setY(float y){
		super.setY(y);
		this.skeleton.setY(y);
	}
	
	public void toggle_state(){
		if(isFruit()){
			this.toBird();
			fruit = !fruit;
		}else if(isAlive()){
			this.toFruit();
			fruit = !fruit;
		}
	}
	
	private void toFruit(){
		state.setAnimation(0, "to_fruit", false);
		state.addAnimation(0, "run_fruit", true, 0);
	}
	
	private void toBird(){
		state.setAnimation(0, "to_bird", false);
		state.addAnimation(0, "run_bird", true, 0);
	}
	
	public void pickItem(){
		if(isFruit())
			return;
		
		state.setAnimation(1, "pick", false);
	}
	
	public void die(){
		if(this.isAlive())
			this.state.setAnimation(0, "die", false);
		
		this.alive = false;
	}
	
	public boolean isAlive(){
		return this.alive;
	}
	
	public boolean isDead() {
		return !this.isAlive();
	}
	
	public boolean isFruit(){
		return this.fruit;
	}
	
	public void dispose(){
		atlas.dispose();
	}
	
	public boolean collides(float x, float y){
		//Make the collision happen at the beginning of the character (far right)
		Vector2 my_pos = new Vector2(this.getX() + this.getWidth()/2, this.getY());
		
		float distance = my_pos.dst2(x, y);
		
		return (distance <= (this.collisionRadius * this.collisionRadius));
	}
}