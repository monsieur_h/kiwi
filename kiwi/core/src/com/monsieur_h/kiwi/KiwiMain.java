package com.monsieur_h.kiwi;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;

public class KiwiMain extends ApplicationAdapter {
	
	private GameScene game;
	
	@Override
	public void create () {
		game = new GameScene();
	}

	@Override
	public void render () {
		game.act(Gdx.graphics.getDeltaTime());
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		game.draw();
	}
	
	@Override
	public void dispose() {
		game.dispose();
	}
}
