package com.monsieur_h.kiwi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;

public class ParallaxManager{
	private List<BackgroundElement> elements = new ArrayList<BackgroundElement>();
	private Vector2 position;
	private float speed;
	private boolean sorted = false;
	
	
	public ParallaxManager() {
		// TODO Auto-generated constructor stub
		this.position = new Vector2(0f, 0f);
		this.speed = 0;
		this.sorted = false;
	}
	
		
	public void setSpeed(float speed){
		this.speed = speed;
	}
	
	public float getSpeed(){
		return this.speed;
	}
	
	public void setPosition(float x, float y){
		this.position = new Vector2(x, y);
	}
	
	public Vector2 getPosition(){
		return this.position;
	}
	
	public void moveBy(float x, float y){
		for( BackgroundElement e : elements ){
			e.moveBy(x, y);
		}
	}
	
	public void add(BackgroundElement element){
		this.elements.add(element);
		this.sorted = false;
	}
	
	public void drawBackground(Batch batch){
		if(!batch.isDrawing())
			batch.begin();
		
		if(!this.isSorted())
			this.sort();
		
		for(BackgroundElement e : elements){
			if(e.getDepth() < 1)
				e.draw(batch, 1.0f);
		}
		batch.end();
	}

	public void drawForeground(Batch batch) {
		if(!batch.isDrawing())
			batch.begin();
		
		if(!this.isSorted())
			this.sort();
		
		for(BackgroundElement e : elements){
			if(e.getDepth() >= 1)
				e.draw(batch, 1.0f);
		}
		batch.end();
	}
	
	private boolean isSorted(){
		return this.sorted;
	}
	
	private void sort(){
		Collections.sort(this.elements);
		this.sorted = true;
	}
	
}
