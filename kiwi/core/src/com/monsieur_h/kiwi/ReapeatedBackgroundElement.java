package com.monsieur_h.kiwi;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;

public class ReapeatedBackgroundElement extends BackgroundElement{
	
	public ReapeatedBackgroundElement(String textureFilename, float depth){
		super(textureFilename, depth);
	}
	
	public ReapeatedBackgroundElement(String textureFilename, float depth,
			boolean fading) {
		super(textureFilename, depth, fading);
	}

	public void draw(Batch batch, float parentAlpha){
		if(this.fading)
			batch.setColor(1 * depth, 1 * depth, 1 * depth, 1);
		
		batch.draw(this.getTexture(), this.getX(), this.getY(), this.getWidth(), this.getHeight());
		
		if(this.getRight() < Gdx.graphics.getWidth()){
			float x = this.getRight();
			while(x < Gdx.graphics.getWidth()){
				batch.draw(this.getTexture(), x, this.getY(), this.getWidth(), this.getHeight());
				x += this.getWidth();
			}
		}else if(this.getX() > 0){
			float x = this.getX();
			while(x > 0){
				batch.draw(this.getTexture(), x, this.getY(), this.getWidth(), this.getHeight());
				x -= this.getWidth();
			}
		}
		batch.setColor(1, 1, 1, 1);
	}
	
	public void moveBy(float x, float y){
		this.setX(this.getX() + (x * this.depth));
		this.setY(this.getY() + (y * this.depth));
		this.replaceRepetition();
	}
	
	protected void replaceRepetition(){
		if(this.getRight() < 0){
			this.setX(this.getX() + this.getWidth());
		}
	}
	
}
