package com.monsieur_h.kiwi;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Actor;


public class Seed extends Actor{
	private final float radius = 10;
	private final Color color = new Color(1, 1, 1, 1);
	private ShapeRenderer renderer;
	private boolean eaten = false;
	
	Seed(){
		super();
		renderer = new ShapeRenderer();
	}
	
	public void draw (Batch batch, float parentAlpha) {
		if(batch.isDrawing())
			batch.end();
		renderer.setProjectionMatrix(batch.getProjectionMatrix());
		
		renderer.begin(ShapeType.Filled);
		renderer.setColor(this.getDrawColor());
		renderer.circle(this.getX(), this.getY(), radius);
		renderer.end();
		
		batch.begin();
	}
	
	protected Color getDrawColor(){
		return this.color;
	}
	
	@Override
	public void act(float delta){
		super.act(delta);
		GameScene g = (GameScene) this.getStage();
		float speed = g.getSpeed();
		this.setX(this.getX() - speed * delta);
	}
	
	public void eaten(){
		this.eaten = true;
		this.remove();
		super.remove();
	}
	
	public boolean isEaten(){
		return this.eaten;
	}
}
